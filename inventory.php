<?php include ('connect_db.php');
session_start();
if (!isset($_POST['itemid'])) $_POST['itemid'] = "";
if (!isset($_POST['Subtype'])) $_POST['Subtype'] = "";
if (!isset($_POST['roomid'])) $_POST['roomid'] = "";
if (!isset($_POST['keyword'])) $_POST['keyword'] = "";
if (!isset($_SESSION["EL_USERNAME"])) $_SESSION["EL_USERNAME"] = "";


error_reporting(E_ALL);
ini_set('display_errors', 1);



$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$stmt2 = $conn->prepare("SELECT * FROM `esci_room` WHERE OWNERFACULTYID = '206' AND ROOMSTATUS = 'N' ");
//$stmt2 = $conn->prepare("SELECT * FROM `sci_room`");
$stmt2->execute();
$rooms = $stmt2->fetchAll();

try {
/*  

  if(isset($_POST["search"])){
    $itemid = $_POST["itemid"];
    $Subtype = $_POST["Subtype"];
    $place = $_POST["place"];
    $strWh = "";
    if($itemid) $strWh .= "Item_id LIKE '%$itemid%' or Subtype LIKE '%$Subtype%' or Place LIKE '%$place%'";
    //$sql = "SELECT * FROM `sci_inventory` WHERE Item_id = '$itemid' or Subtype = '$Subtype' or Place = '$place'";
    $sql = "SELECT * FROM `sci_inventory` WHERE id is not null LIMIT 0, 50";
    $stmt1 = $conn->prepare($sql);
    $stmt1->execute();
    $inventory = $stmt1->fetchAll();
    }else{
    $stmt = $conn->prepare("SELECT * FROM `sci_inventory` LIMIT 0, 50");
    $stmt->execute();
    $inventory = $stmt->fetchAll();
    //echo $room ['Room_id'];
    }
*/
    //$itemid = $_POST["itemid"];
    //$Subtype = $_POST["Subtype"];
    $keyword = trim($_POST["keyword"]);
    $roomid = $_POST["roomid"];
    $strWh = "";
    //if($itemid) $strWh .= " AND Item_id LIKE '%$itemid%' ";
    //if($Subtype) $strWh .= "AND Subtype LIKE '%$Subtype%' ";

    if($roomid) $strWh .= "AND Room_id = $roomid ";
    if($keyword){
        $strWh .= " AND (
                        Item_id LIKE '%$keyword%'
                        OR Subtype LIKE '%$keyword%'
                        OR Place LIKE '%$keyword%'
                        OR Serial_no LIKE '%$keyword%'
            )
        ";
    }
    $sql = "SELECT * FROM sci_inventory WHERE id is not null $strWh ORDER BY Item_id LIMIT 0, 50";
    //echo "<br>==>".$sql;
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $inventory = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>รายการครุภัณฑ์</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/logo-sci-01.png" rel="icon">
    <link href="assets/img/logo-sci-01.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Mentor
  * Updated: Sep 18 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">

            <h1 class="logo me-auto"><a href="index.php">inventory</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

            <nav id="navbar" class="navbar order-last order-lg-0">
                <ul>
                    <li><a class="active" href="inventory.php">รายการครุภัณฑ์</a></li>
                    <li><a href="printbarcode.php">พิมพ์บาร์โค้ด</a></li>
                    <li><a href="report.php">รายงานการสำรวจ</a></li>
                    <li><a href="reportstatus.php">รายงานสถานะ</a></li>
                    <li><a></a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->
            <button name="aa" class="btn btn-success"><?php echo $_SESSION['EL_USERNAME'];?></button>
            <a href="logout.php" class="btn btn-danger">Log out</a>
        </div>
    </header><!-- End Header -->
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs" data-aos="fade-in">
            <div class="container">
                <h2>รายการครุภัณฑ์</h2>
                <p>คณะวิทยาศาสตร์ มหาวิทยาลัยนเรศวร </p>
            </div>
        </div><!-- End Breadcrumbs -->

        <div class="container center">
            <!--<style>
            h4 {
                text-align: center;
            }
            </style>-->
            <form name="frmSearch" method="POST" enctype='multipart/form-data'>
            <h4 style="margin-top: 30px;text-align: center">ค้นหาครุภัณฑ์</h>                
            <div class="row">
        <!--   
                <div class="col-md-4 form-group mt-2">
                        <label class="form-control-label">รหัสครุภัณฑ์</label>
                        <input type="text" name="itemid" class="form-control" placeholder="รหัสครุภัณฑ์" value="<?php echo $itemid;?>">
                    </div>
                    <div class="col-md-4 form-group mt-2">
                        <label class="form-control-label">ชื่อครุภัณฑ์</label>
                        <input type="text" name="Subtype" class="form-control" placeholder="ชื่อครุภัณฑ์" value="<?php echo $Subtype;?>">
                    </div>
        -->
                    <!--
                        <div class="col-md-4 form-group mt-2">
                        <label class="form-control-label">ห้องที่ใช้งาน</label>
                        <input type="text" name="place"
                            class="form-control" placeholder="ห้องที่ใช้งาน" value="">
                    </div>
                    -->
                    <div class="col-md-2 form-group mt-2">
                    </div>
                    <div class="col-md-4 form-group mt-2" align="left">
                        <b style="font-size: 14px;">ห้องที่เก็บครุภัณฑ์</b>
                        <select class="form-select" name="roomid">
                            <option value="">เลือกห้อง</option>
                            <?php
                             foreach($rooms as $room){
                            ?>
                            <option value="<?php echo $room["ROOMID"];?>" <?php if($room["ROOMID"] == $roomid){echo "selected";}?>><?php echo $room['ROOMCODE'];?> : <?php echo $room['ROOMNAME'];?></option>
                            <?php }
                ?>
                        </select>
                    </div>
                    <div class="col-md-4 form-group mt-2" align="left">
                        <b style="font-size: 14px;">คำค้น</b>
                        <input type="search" name="keyword" class="form-control" placeholder="ชื่อครุภัณฑ์ / รหัสครุภัณฑ์ / SN" value="<?php echo $keyword;?>">
                    </div>

                    <div align="left" class="col-md-2 form-group mt-2">
                        <br>
                        <button type="submit" name="search" class="btn btn-success">ค้นหาครุภัณฑ์</button>
                    </div>
                </div>

        </div>
        <style>
        table {
            text-align: center;
            font-size: 13px;
        }

        th {
            vertical-align: middle;
        }

        .btn {
            font-size: 14px;
        }
        </style>
        <div class="table-responsive">
            <table class="table container  mt-3 table-bordered table-striped">
                <thead class="thead-dark table-success">
                    <tr>
                        <th scope="col" rowspan="3">#</th>
                        <th scope="col" rowspan="3">รหัสครุภัณฑ์</th>
                        <th scope="col" rowspan="3">รหัสครุภัณฑ์ 3 มิติ</th>
                        <th scope="col" rowspan="3">ชื่อครุภัณฑ์</th>
                        <th scope="col" rowspan="3">S/N</th>
                        <th scope="col" colspan="2" rowspan="2">ได้มา</th>
                        <th scope="col" rowspan="3">ราคาต่อหน่วย</th>
                        <th scope="col" rowspan="3">เลขที่ใบเบิก</th>
                        <th scope="col" rowspan="3">วิธีการได้มา</th>
                        <th scope="col" rowspan="3">ผู้ใช้งาน</th>
                        <th scope="col" colspan="4" text-center>สภาพการสำรวจ</th>
                        <th scope="col" rowspan="3">ใช้อยู่ที่</th>
                        <th scope="col" rowspan="3">ทะเบียนครุภัณฑ์</th>
                        <th scope="col" rowspan="3">Action</th>
                    </tr>
                    <tr>
                        <th scope="col" colspan="2">ใช้ได้</th>
                        <th scope="col" colspan="2">ใช้ไม่ได้</th>
                    </tr>
                    <tr>
                        <th scope="col">เมื่อไร</th>
                        <th scope="col">ประเภทงบประมาณ</th>
                        <th scope="col">ใช้งานต่อ</th>
                        <th scope="col">จำหน่ายปี</th>
                        <th scope="col">ซ่อมใช้งานต่อ</th>
                        <th scope="col">จำหน่ายปี</th>
                    </tr>
                </thead>
                <tbody>
<?php
                $countInv = 1;
                foreach($inventory as $inventory){
                $rDate = $inventory["R_date"]."/".$inventory["R_month"]."/".$inventory["R_year"];

                $roomName = "-";
                if($inventory["Room_id"]){
                    $sqlRoom = "SELECT * FROM esci_room WHERE ROOMID = ".$inventory["Room_id"];
                    //echo "<br>".$sqlRoom;
                    $stmtRoom = $conn->prepare($sqlRoom);
                    $stmtRoom->execute();
                    $roomArr = $stmtRoom->fetch();
                    $roomName = $roomArr["ROOMCODE"];
                }
?>
                    <tr style="text-align: left;">
                        <td><?php echo $countInv;?></td>
                        <td scope="row">
<!--
                    <p><button type="button" class="btn btn-primary">Update</button></p>
-->

                        
<?php 
                        echo $inventory['Item_id'];

                        $barcodeText = trim($inventory['Item_id']);
                        $barcodeType = "code128";
                        $barcodeDisplay = "horizontal";
                        $barcodeSize = 20;
                        $printText = "true";
                        if($barcodeText != '') {
                            echo '<h4>Barcode:</h4>';
                            echo '<img class="barcode" alt="'.$barcodeText.'" src="barcode.php?text='.$barcodeText.'&codetype='.$barcodeType.'&orientation='.$barcodeDisplay.'&size='.$barcodeSize.'&print='.$printText.'"/>';
                        }
?>
                        
                        </td>
                        <td><?php echo $inventory['Initem_id'];?></td>
                        <td><?php echo $inventory["Subtype"];?></td>
                        <td><?php echo $inventory["Serial_no"];?></td>
                        <td align="center"><?php echo $rDate;?></td>
                        <td><?php echo $inventory["Budget_type"];?></td>
                        <td align="right"><?php echo number_format($inventory["Cost"]);?></td>
                        <td align="center"><?php echo $inventory["Buy_id"];?></td>
                        <td align="center"><?php echo $inventory["Method"];?></td>
                        <td><?php echo $inventory["Provider"];?></td>
                        <td><?php if ($inventory["Status"] == 'ดี'){
                            echo $inventory["Status"];
                        }else{
                            echo '-';
                        }
                        //echo $inventory["Status"];?></td>
                        </td>
                        <td><?php if ($inventory["Status"] == 'ดี'){
                            //echo 'ดี';
                            if ($inventory["Auc_year"] != ''){
                                echo $inventory["Auc_year"];
                                
                            }else{
                                echo '-';
                            }
                        }if ($inventory["Status"] == 'ชำรุด'){
                                echo '-';
                        }
                        //echo $inventory["Auc_year"];?></td>
                        </td>
                        <td><?php if ($inventory["Status"] == 'ชำรุด'){
                            echo $inventory["Status"];
                        }else{
                            echo '-';
                        }
                        // echo $inventory["Status"];?></td>
                        </td>
                        <td><?php if ($inventory["Status"] == 'ชำรุด'){
                            //echo 'ดี';
                            if ($inventory["Auc_year"] != ''){
                                echo $inventory["Auc_year"];
                                
                            }else{
                                echo '-';
                            }
                        }else{
                            echo '-';
                        }// echo $inventory["Auc_year"];?></td>
                        </td>
                        <td><?php echo $roomName;?></td>
                        <td><?php echo $inventory["Job_Detail"];?></td>
                        <td align="center">
                            <div class="btn-group">
                                <!--<a href="editinventory.php?id=<?php echo $inventory['id']; ?>"
                                    class="btn btn-sm btn-info">Detail</a>
                                <a href="deleteinventory_db.php?id=<?php echo $inventory['id']; ?>"
                                    onclick="return confirm('ยืนยันการลบ')" class="btn btn-sm btn-danger">Delete</a>-->
                                <button type="button" class="btn btn-sm btn-warning" name="btn_edit" onclick="gotoEdit(<?php echo $inventory['id']; ?>);">Edit</button>

                            </div>
                        </td>
                    </tr>

                        <?php
                        $countInv++;
                                }
                            }
                    catch(PDOException $e) {
                    echo "Error: " . $e->getMessage();
                    }
                    
?>
                </tbody>
            </table>
        </form>
        <form name="frm_edit" method="post" action="editinventory.php">
            <input type="hidden" name="inv_id">
        </form>
        </div>



        <!-- ======= Footer ======= -->
        <footer id="footer">

            <div class="footer-top">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-md-6 footer-contact">
                            <h5>เกี่ยวกับคณะวิทยาศาสตร์</h5>
                            <p>คณะวิทยาศาสตร์ มหาวิทยาลัยนเรศวร <br>
                                ประกอบด้วย สำนักงานเลขานุการ และ <br>
                                5 ภาควิชาได้แก่ ภาควิชาคณิตศาสตร์ <br>
                                ภาควิชาเคมี ภาควิชาชีววิทยา ภาควิชาฟิสิกส์ <br>
                                และภาควิชาวิทยาการคอมพิวเตอร์และ<br>
                                เทคโนโลยีสารสนเทศ<br>
                                <strong>ที่อยู่:</strong> 99 หมู่ 9 ตำบล ท่าโพธิ์ อำเภอเมือง จังหวัด <br>
                                พิษณุโลก 65000<br>
                                <strong>โทรศัพท์:</strong> 055-963112<br>
                                <strong>โทรสาร:</strong> 055-963113<br>
                                <strong>E-mail:</strong> saraban_sci@nu.ac.th<br>
                            </p>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Our Services</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-4 col-md-6 footer-newsletter">
                            <h4>Join Our Newsletter</h4>
                            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                            <form action="" method="post">
                                <input type="email" name="email"><input type="submit" value="Subscribe">
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="container d-md-flex py-4">

                <div class="me-md-auto text-center text-md-start">
                    <div class="copyright">
                        &copy; Copyright <strong><span>Mentor</span></strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                        <!-- All the links in the footer should remain intact. -->
                        <!-- You can delete the links only if you purchased the pro version. -->
                        <!-- Licensing information: https://bootstrapmade.com/license/ -->
                        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/ -->
                        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                    </div>
                </div>
                <div class="social-links text-center text-md-right pt-3 pt-md-0">
                    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                </div>
            </div>
        </footer><!-- End Footer -->

        <div id="preloader"></div>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
                class="bi bi-arrow-up-short"></i></a>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
        <script src="assets/vendor/aos/aos.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>
<script type="text/javascript">
    function gotoEdit(inv_id){
        document.frm_edit.inv_id.value = inv_id; 
        document.frm_edit.submit(); 
    }
</script>
</body>

</html>