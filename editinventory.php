<?php include ('connect_db.php');
session_start();

if(!isset($_SESSION["EL_USERNAME"])) $_SESSION["EL_USERNAME"] = "";
error_reporting(E_ALL);
ini_set('display_errors', 1);
if (!isset($_POST['roomid'])) $_POST['roomid'] = "";
$roomid = $_POST["roomid"];
/*
if (!isset($_REQUEST['itemid'])) $_REQUEST['itemid'] = "";
if (!isset($_REQUEST['initemid'])) $_REQUEST['initemid'] = "";
if (!isset($_REQUEST['subtype'])) $_REQUEST['subtype'] = "";
if (!isset($_REQUEST['ryear'])) $_REQUEST['ryear'] = "";
if (!isset($_REQUEST['budgettype'])) $_REQUEST['budgettype'] = "";
if (!isset($_REQUEST['cost'])) $_REQUEST['cost'] = "";
if (!isset($_REQUEST['buyid'])) $_REQUEST['buyid'] = "";
if (!isset($_REQUEST['method'])) $_REQUEST['method'] = "";
if (!isset($_REQUEST['depname'])) $_REQUEST['depname'] = "";
if (!isset($_REQUEST['place'])) $_REQUEST['place'] = "";
if (!isset($_REQUEST['jobdetail'])) $_REQUEST['jobdetail'] = "";
if (!isset($_REQUEST['serialno'])) $_REQUEST['serialno'] = "";
*/
$id = $_POST['inv_id'];
/*
$itemid = $_REQUEST['itemid'];
$initemid = $_REQUEST['initemid'];
$subtype = $_REQUEST['subtype'];
$serialno = $_REQUEST['serialno'];
$ryear = $_REQUEST['ryear'];
$budgettype = $_REQUEST['budgettype'];
$cost = $_REQUEST['cost'];
$buyid = $_REQUEST['buyid'];
$method = $_REQUEST['method'];
$depname = $_REQUEST['depname'];
$place = $_REQUEST['place'];
$jobdetail = $_REQUEST['jobdetail'];
*/
        $date = date("Y-m-d");
        try {
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT * FROM `sci_inventory` WHERE id = $id");
            $stmt->execute();
            $inventory = $stmt->fetch();
            //ใช้ Sciroom
            $stmt1 = $conn->prepare("SELECT * FROM `esci_room` WHERE OWNERFACULTYID = '206' AND ROOMSTATUS = 'N' ");
            $stmt1->execute();
            $rooms = $stmt1->fetchAll();
        } catch(PDOException $e) {
            //echo $e->getMessage();
          }
          
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>แก้ไขครุภัณฑ์</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/logo-sci-01.png" rel="icon">
    <link href="assets/img/logo-sci-01.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Mentor
  * Updated: Sep 18 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">

            <h1 class="logo me-auto"><a href="index.php">inventory</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

            <nav id="navbar" class="navbar order-last order-lg-0">
                <ul>
                <li><a href="inventory.php">รายการครุภัณฑ์</a></li>
                    <li><a class="active" href="printbarcode.php">พิมพ์บาร์โค้ด</a></li>
                    <li><a href="report.php">รายงานการสำรวจ</a></li>
                    <li><a href="reportstatus.php">รายงานสถานะ</a></li>
                    <li><a></a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

            <button name="aa" class="btn btn-success"><?php echo $_SESSION['EL_USERNAME'];?></button>
            <a href="logout.php" class="btn btn-danger">Log out</a>

        </div>
    </header><!-- End Header -->

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
        <div class="container">
            <h2>แก้ไขครุภัณฑ์</h2>
            <p>คณะวิทยาศาสตร์ มหาวิทยาลัยนเรศวร </p>
        </div>
    </div><!-- End Breadcrumbs -->

    <div class="container col-lg-8 mt-3">

        <form action="editinventory_db.php" method="post">
        <div class = "row">
        <div class="col-md-6 form-group mt-4">

        <?php //echo $inventory['Item_id'];
                                
                                $barcodeText = trim($inventory['Item_id']);
                                $barcodeType="code128";
                                $barcodeDisplay="horizontal";
                                $barcodeSize=30;
                                $printText="true";
                                if($barcodeText != '') {
                                    echo '<h4>Barcode:</h4>';
                                    echo '<img class="barcode" alt="'.$barcodeText.'" src="barcode.php?text='.$barcodeText.'&codetype='.$barcodeType.'&orientation='.$barcodeDisplay.'&size='.$barcodeSize.'&print='.$printText.'"/>';
                                }?>
            </div>
        </div>
            <div class="row">
                <div class="col-md-6 form-group mt-2">
                    <input type="hidden" name="id" class="form-control" value="<?php echo $inventory['id'];?>" required>
                    <label class="form-control-label">รหัสครุภัณฑ์</label><input type="text" name="itemid"
                        class="form-control" placeholder="รหัสครุภัณฑ์" value="<?php echo $inventory['Item_id'];?>"
                        required disabled>
                    <input type="hidden" name="itemid" class="form-control" placeholder="รหัสครุภัณฑ์"
                        value="<?php echo $inventory['Item_id'];?>" required>
                </div>
                <div class="col-md-6 form-group mt-2">
                    <label class="form-control-label">รหัสครุภัณฑ์ 3 มิติ</label><input type="text" class="form-control"
                        name="initemid" placeholder="รหัสครุภัณฑ์ 3 มิติ" value="<?php echo $inventory['Initem_id'];?>"
                        required disabled>
                    <input type="hidden" class="form-control" name="initemid" placeholder="รหัสครุภัณฑ์ 3 มิติ"
                        value="<?php echo $inventory['Initem_id'];?>" required>
                </div>
                <div class="mt-3">
                    <label class="form-control-label">ชื่อครุภัณฑ์</label><input type="text" class="form-control"
                        name="subtype" placeholder="ชื่อครุภัณฑ์" value="<?php echo $inventory['Subtype'];?>" required
                        disabled>
                    <input type="hidden" class="form-control" name="subtype" placeholder="ชื่อครุภัณฑ์"
                        value="<?php echo $inventory['Subtype'];?>" required>
                </div>
                <div class="mt-3">
                    <label class="form-control-label">S/N</label><input type="text" class="form-control" name="serialno"
                        placeholder="S/N" value="<?php echo $inventory['Serial_no'];?>" required disabled>
                    <input type="hidden" class="form-control" name="serialno" placeholder="S/N"
                        value="<?php echo $inventory['Serial_no'];?>" required>
                </div>
                <div class="col-md-4 form-group mt-3">
                    <label class="form-control-label">ได้มาเมื่อ</label><input type="text" name="ryear"
                        class="form-control" placeholder="ได้มาเมื่อ" value="<?php echo $inventory['R_year'];?>"
                        required disabled>
                    <input type="hidden" name="ryear" class="form-control" placeholder="ได้มาเมื่อ"
                        value="<?php echo $inventory['R_year'];?>" required>
                </div>
                <div class="col-md-8 form-group mt-3">
                    <label class="form-control-label">ประเภทงบประมาณ</label><input type="text" class="form-control"
                        name="budgettype" placeholder="ประเภทงบประมาณ" value="<?php echo $inventory['Budget_type'];?>"
                        required disabled>
                    <input type="hidden" class="form-control" name="budgettype" placeholder="ประเภทงบประมาณ"
                        value="<?php echo $inventory['Budget_type'];?>" required>
                </div>
                <div class="col-md-6 form-group mt-3">
                    <label class="form-control-label">ราคาต่อหน่วย</label><input type="number" name="cost"
                        class="form-control" placeholder="ราคาต่อหน่วย" value="<?php echo $inventory['Cost'];?>"
                        required disabled>
                    <input type="hidden" name="cost" class="form-control" placeholder="ราคาต่อหน่วย"
                        value="<?php echo $inventory['Cost'];?>" required>
                </div>
                <div class="col-md-6 form-group mt-3">
                    <label class="form-control-label">เลขที่ใบเบิก</label><input type="text" class="form-control"
                        name="buyid" placeholder="เลขที่ใบเบิก" value="<?php echo $inventory['Buy_id'];?>" required
                        disabled>
                    <input type="hidden" class="form-control" name="buyid" placeholder="เลขที่ใบเบิก"
                        value="<?php echo $inventory['Buy_id'];?>" required>
                </div>
                <div class="col-md-6 form-group mt-3">
                <label class="form-control-label">วิธีการได้มา</label>
                <input type="text" class="form-control" name="Method" value="<?php echo $inventory['Method'];?>" required disabled>
                <input type="hidden" class="form-control" name="buyid" placeholder="Method" value="<?php echo $inventory['Buy_id'];?>" required>
                </div>
                <div class="col-md-6 form-group mt-3">
                    <label class="form-control-label">ผู้ใช้งาน</label><input type="text" class="form-control"
                        name="depname" placeholder="ผู้ใช้งาน" value="<?php echo $inventory['Provider'];?>">
                </div>
                <div>
                    <label>สถานะ</label>
                    <div>
                        <p><input name="status" class="form-check-input" type="radio" value="ดี" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">ดี</label>
                        <p><input name="status" class="form-check-input" type="radio" value="ชำรุด" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">ชำรุด</label>
                        <p><input name="status" class="form-check-input" type="radio" value="สูญหาย" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">สูญหาย</label>
                    </div>
                </div>
                <div>
                    <label>จำหน่ายปี</label>
                    <input type="text" name="place" class="form-control" placeholder="ปี"
                        value="<?php echo $inventory['Auc_year'];?>">
                </div>

                <div class="col-md-6 form-group mt-3">
                    <label class="form-control-label">ใช้อยู่ที่</label>
                    <select class="form-select" name="place">
                    <option value="">เลือกห้อง</option>
                            <?php
                             foreach($rooms as $room){
                            ?>
                            <option value="<?php echo $room["ROOMID"];?>" <?php if($room["ROOMID"] == $roomid){echo "selected";}?>><?php echo $room['ROOMCODE'];?> : <?php echo $room['ROOMNAME'];?></option>
                        <?php }
                ?>
                    </select>
                </div>

                <div class="col-md-6 form-group mt-3">
                    <label class="form-control-label">ทะเบียนครุภัณฑ์</label>
                    <input type="text" class="form-control" name="jobdetail" placeholder="ทะเบียนครุภัณฑ์" value="<?php echo $inventory['Job_Detail'];?>" disabled>
                    <input type="hidden" class="form-control" name="jobdetail" placeholder="ทะเบียนครุภัณฑ์" value="<?php echo $inventory['Job_Detail'];?>">
                </div>
                <div class="mt-2 text-center">
                    <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                    <td>
                    </td>

                </div>

        </form>

    </div>
    </div>



    <!-- ======= Footer ======= -->
    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h5>เกี่ยวกับคณะวิทยาศาสตร์</h5>
                        <p>คณะวิทยาศาสตร์ มหาวิทยาลัยนเรศวร <br>
                            ประกอบด้วย สำนักงานเลขานุการ และ <br>
                            5 ภาควิชาได้แก่ ภาควิชาคณิตศาสตร์ <br>
                            ภาควิชาเคมี ภาควิชาชีววิทยา ภาควิชาฟิสิกส์ <br>
                            และภาควิชาวิทยาการคอมพิวเตอร์และ<br>
                            เทคโนโลยีสารสนเทศ<br>
                            <strong>ที่อยู่:</strong> 99 หมู่ 9 ตำบล ท่าโพธิ์ อำเภอเมือง จังหวัด <br>
                            พิษณุโลก 65000<br>
                            <strong>โทรศัพท์:</strong> 055-963112<br>
                            <strong>โทรสาร:</strong> 055-963113<br>
                            <strong>E-mail:</strong> saraban_sci@nu.ac.th<br>
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Our Services</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-newsletter">
                        <h4>Join Our Newsletter</h4>
                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                        <form action="" method="post">
                            <input type="email" name="email"><input type="submit" value="Subscribe">
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div class="container d-md-flex py-4">

            <div class="me-md-auto text-center text-md-start">
                <div class="copyright">
                    &copy; Copyright <strong><span>Mentor</span></strong>. All Rights Reserved
                </div>
                <div class="credits">
                    <!-- All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: https://bootstrapmade.com/license/ -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/ -->
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
            </div>
            <div class="social-links text-center text-md-right pt-3 pt-md-0">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>