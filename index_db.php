<?php
session_start();
include("connect_db.php");
// ตรวจสอบการ submit และตรวจสอบข้อมูลที่ส่งมาจากฟอร์ม
if(isset($_POST['login'])) {
    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';

    // ตรวจสอบข้อมูลเข้าสู่ระบบ (ในที่นี้คือตัวอย่างเท่านั้น)
$authen = false;
    
    $sql = "select * from sci_user where username = ? and password = ? ";
//echo $sql;exit;
try{
    $stmt = $conn->prepare($sql);
    $stmt->execute([$username,$password]);
    $rows = $stmt->Fetch(); 
    if($rows){
        $authen = true;
        $_SESSION["EL_USERNAME"] = $rows["username"];
    }
} 

    catch(Exception $e){
    print_r($conn->errorInfo());
}

}
?>
<script type="text/javascript">
<?php
    if(!$authen){
?>
        alert("Login fail.");
        window.location = "index.php";
<?php
    }else{
?>
        window.location = "inventory.php";
<?php 
    }
?>
</script>