<?php
session_start();
session_destroy();
unset($_SESSION["EL_USERNAME"]);
?>
<script type="text/javascript">
    window.location = "index.php";
</script>